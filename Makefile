ifeq ($(PREFIX),)
    PREFIX := /usr/local
endif

install:
	install -m 755 compress-h265 $(PREFIX)/bin/
	install -m 755 compress-h265-report $(PREFIX)/bin/
	install -m 755 my-mpv $(PREFIX)/bin/
	install -m 755 train_img_info_classifier $(PREFIX)/bin/
