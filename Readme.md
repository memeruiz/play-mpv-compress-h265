# My-mpv and compress-h265

MPV playlist player and video collection h265 compressor util

There are three applications:

my-mpv: media player using playlists and mpv

compress-h265: recompress to h265 all playlist files, using ffmpeg and mpv+
    imagemagick for testing results

train_img_info_classifier: Creates a k-nearest-neighbor machine learning from 
    image examples of "high" or "low" image information content. This is used 
    by compress-h265 to select three video snapshots to compare original video 
    to resulting video

## Features:

### my-mpv:

- Takes m3u style playlists as input and generates an internal more flexible playlist representation (based on md5)
- There is a preferred playlist and a main playlist. Select and store items in playlists using direct keys
- Using similar mpv keybindings for video navigation and volume
- Generates a new playlist filename that has more information (can contain multiple playlists, current playlist position, this can be easily expanded)

### compress-h265
- Takes mp-mpv playlist and tries to convert all video files into h265 video codec leaving audio as the original file.
- You can set a maximum resolution and any file in the playlist that is bigger will be resampled (even if it is a h265 file already)
- Uses imagemagick image processing tools to check that the resulting new video files are similar to the original file by taking three snapshots at around 10% 40% and 98% of the video position.
- It checks that the snapshots have "enough information", that is, not simple black or near black images, so that it only uses "useful" snapshots. The program firsts starts at 10%, 40% and 98% and it moves away from this positions until it finds useful snapshots in the original file, then it compares this snapshots to snapshots in the same position of the output file and in case that they are sufficiently similar then it replaces the file.
- It is possible to choose the replacement behavoir of the new files (remove old and replace with new, leave both)
- If checking fails the original and resulting files are left for the user to check them.
- It uses a trainable k-nearest-neighbor algorithm to learn to classify according to user preference, what is a snapshot with sufficient information or not.
- Can use Intel QSV and Nvidia hardware acceleration for decoding and encoding (it uses ffmpeg under the hood)
- Many more I don't remember. This was programmed in a hurry ... :/

## Usage guide

### my-mpv

TODO

### compress-h265

TODO


## Installation instructions:

pip3 install python-mpv python-sklearn
apt-get install ffmpeg mpv imagemagick

## References

Similar utility but written in java (doesn't have result checking as far as I'm aware)

https://github.com/FallingSnow/h265ize

https://www.ffmpeg.org/ffmpeg-codecs.html

https://obsproject.com/forum/resources/custom-parameters-of-quicksync.104/

https://github.com/mpv-player/mpv/blob/master/DOCS/man/input.rst

https://superuser.com/questions/1236275/how-can-i-use-crf-encoding-with-nvenc-in-ffmpeg

https://forums.developer.nvidia.com/t/crf-for-nvenc/50616/3

https://github.com/HandBrake/HandBrake/issues/2231

https://trac.ffmpeg.org/wiki/Hardware/QuickSync

https://trac.ffmpeg.org/wiki/Hardware/VAAPI

https://trac.ffmpeg.org/wiki/HWAccelIntro
